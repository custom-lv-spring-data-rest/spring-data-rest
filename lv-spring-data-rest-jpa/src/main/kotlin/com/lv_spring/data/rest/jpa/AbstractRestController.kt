package com.lv_spring.data.rest.jpa

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.BeanUtils
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.data.domain.Sort.Order
import org.springframework.data.jpa.domain.Specification
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

abstract class AbstractRestController<T : Any, ID : Any>(
    protected val repository: JpaRepositoryAndSpecificationExecutor<T, ID>
) {
    companion object {
        private val logger: Logger = LoggerFactory.getLogger(AbstractRestController::class.java)
    }

    @GetMapping("{id}")
    @Throws(ResourceNotFoundException::class)
    fun getById(
        @PathVariable id: ID,
        @RequestParam(name = "\$expand", defaultValue = "") expand: String? = null,
        @RequestParam(name = "\$select", defaultValue = "") select: String? = null,
    ): T {
        logger.debug("expand: {}", expand)
        logger.debug("select: {}", select)
        return repository.findById(id)
            .orElseThrow { ResourceNotFoundException() }
    }


    @GetMapping
    fun findAll(
        @RequestParam(defaultValue = "\${lv_spring.data.rest.jpa.page.size:20}") size: Int,
        @RequestParam(defaultValue = "\${lv_spring.data.rest.jpa.page.number:0}") page: Int,
        @RequestParam(defaultValue = "") sortBy: Set<String>,
        @RequestParam(name = "\$expand", defaultValue = "") expand: String,
        @RequestParam(name = "\$select", defaultValue = "") select: String,
    ): Any? {
        logger.debug("expand: {}", expand)
        logger.debug("select: {}", select)

        return findAllByQuery(size, page, sortBy)
    }

    protected open fun findAllByQuery(
        size: Int,
        page: Int,
        sortBy: Set<String>,
        query: Specification<T>? = null,
    ): Any? {
        val orders = sortBy.map {
            val orderDir = it.split(" ", "_", "-")
            val order = orderDir[0]
            val direction =
                if (orderDir.size > 1) orderDir[1].uppercase()
                else "ASC"
            Order.by(order)
                .with(Sort.Direction.valueOf(direction))
        }
        val sort = Sort.by(orders)
        logger.debug("sort: {}", sort)

        return if (query != null) {
//            val query = RSQLJPASupport.toSpecification<T>(search)

            when (size) {
                0 -> repository.findAll(query, sort)
                1 -> repository.findAll(query, PageRequest.of(0, 1)).content[0]
                else -> repository.findAll(query, PageRequest.of(page, size, sort))
            }
        } else when (size) {
            0 -> repository.findAll(sort)
            1 -> repository.findAll(PageRequest.of(0, 1)).content[0]
            else -> repository.findAll(PageRequest.of(page, size, sort))
        }
    }


    protected open fun save(entity: T): T {
        return repository.save(entity)
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun createOne(
        @RequestBody entity: T,
        @RequestParam(name = "\$expand", defaultValue = "") expand: String? = null,
        @RequestParam(name = "\$select", defaultValue = "") select: String? = null,
    ): T {
        logger.debug("expand: {}", expand)
        logger.debug("select: {}", select)
        logger.debug("create() with body {} of type {}", entity, entity::class)

        return save(entity)
    }

    @PutMapping
    fun updateOne(
        @RequestBody item: T,
        @RequestParam(name = "\$expand", defaultValue = "") expand: String? = null,
        @RequestParam(name = "\$select", defaultValue = "") select: String? = null,
    ): T {
        logger.debug("expand: {}", expand)
        logger.debug("select: {}", select)
        logger.debug("update() with body {}", item)

        return save(item)
    }

    @PatchMapping("/{id}")
    @Throws(ResourceNotFoundException::class)
    fun patchOne(
        @PathVariable id: ID,
        @RequestBody item: Map<String, Any>,
        @RequestParam(name = "\$expand", defaultValue = "") expand: String? = null,
        @RequestParam(name = "\$select", defaultValue = "") select: String? = null,
    ): T {
        logger.debug("expand: {}", expand)
        logger.debug("select: {}", select)
        logger.debug("patch() of id#{} with body {}", id, item)
        logger.debug("T json is of type {}", item::class)

        val entity = getById(id)
        try {
            BeanUtils.copyProperties(entity, item)
        } catch (e: Exception) {
            logger.warn("Error while copying properties", e)
        }

        logger.debug("merged entity: {}", entity)

        return save(entity)
    }

    @DeleteMapping("/{id}")
    fun deleteById(@PathVariable id: ID) {
        repository.deleteById(id)
    }
}
