package com.lv_spring.data.rest.jpa

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class ResourceNotFoundException @JvmOverloads constructor(
    message: String? = "Entity not found!",
    cause: Throwable? = null
) :
    RuntimeException(message, cause) {
    companion object {
        private const val serialVersionUID = 7992904489502842099L
    }
}
