grammar Expand;

@header {
package com.lv_spring.data.rest.jpa.expand;
}

expand      : expandItem (',' expandItem)*;
expandItem
    : PROPERTY '(' selectEq (';' expandEq)? ')' # ExpandOptionalSelectExpand
    | PROPERTY '(' expandEq (';' selectEq)? ')' # ExpandOptionalExpandSelect
    | PROPERTY '(' expand (';' select)? ')'     # ExpandExpandSelect
    | PROPERTY ('.' PROPERTY)*                  # ExpandPropertyPath
    ;

expandEq    : EXPAND '=' expand;
selectEq    : SELECT '=' select;
select      : PROPERTY (',' PROPERTY)*;

fragment LETTER  : [A-Za-z] ;
fragment DITGIT      : [0-9]+;

EXPAND      : '$expand';
SELECT      : '$select';

PROPERTY    : (LETTER | '_' | '$') (LETTER | '_' | DITGIT | '$')+;

WHITESPACE  : ' ' -> skip ;
