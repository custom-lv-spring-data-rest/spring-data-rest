package com.lv_spring.data.rest.jpa.demo.stepDefinitions

import io.cucumber.java.en.When
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class RestAPIStepsDefinition(private val mockMvc: MockMvc) {

    @When("GET {string}, the server should respond with status {int} and the json response should be")
    fun userGET(url: String, statusCode: Int, jsonResponse: String) {
        mockMvc.get(url)
                .andDo { print() }
            .andExpect {
                status { isEqualTo(statusCode) }
                content {
                    if (jsonResponse.isBlank()) string("")
                    else json(jsonResponse, true)
                }
            }
    }

    @When("POST {string}, with content: {string}, the server should respond with status {int} and the json response should be")
    fun userPOST(url: String, contentVal: String, statusCode: Int, jsonResponse: String) {
        mockMvc.post(url) {
            content = contentVal
            contentType = MediaType.APPLICATION_JSON
        }
                .andDo { print() }
            .andExpect {
                status { isEqualTo(statusCode) }
                content {
                    if (jsonResponse.isBlank()) string("")
                    else json(jsonResponse, true)
                }
            }
    }

    @When("PUT {string}, with content: {string}, the server should respond with status {int} and the json response should be")
    fun userPUT(url: String, contentVal: String, statusCode: Int, jsonResponse: String) {
        mockMvc.put(url) {
            content = contentVal
            contentType = MediaType.APPLICATION_JSON
        }
                .andDo { print() }
            .andExpect {
                status { isEqualTo(statusCode) }
                content {
                    if (jsonResponse.isBlank()) string("")
                    else json(jsonResponse, true)
                }
            }
    }

    @When("DELETE {string}, the server should respond with status {int} and the json response should be")
    fun userDELETE(url: String, statusCode: Int, jsonResponse: String) {
        mockMvc.delete(url)
            .andExpect {
                status { isEqualTo(statusCode) }
                content {
                    if (jsonResponse.isBlank()) string("")
                    else json(jsonResponse, true)
                }
            }
    }
}
