package com.lv_spring.data.rest.jpa.demo

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer


@SpringBootApplication
class LvSpringDataRestJpaDemoApplication : WebMvcConfigurer {
    override fun addViewControllers(registry: ViewControllerRegistry) {
        registry.addRedirectViewController("/", "/swagger-ui/index.html")
    }
}

fun main(args: Array<String>) {
    runApplication<LvSpringDataRestJpaDemoApplication>(*args)
}
