package com.lv_spring.data.rest.jpa.demo.data

import com.lv_spring.data.rest.jpa.demo.models.Author
import com.lv_spring.data.rest.jpa.demo.models.Book
import com.lv_spring.data.rest.jpa.demo.repositories.AuthorRepository
import com.lv_spring.data.rest.jpa.demo.repositories.BookRepository
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Component

@Component
@Profile("default")
class InitDb(
        bookRepository: BookRepository,
        authorRepository: AuthorRepository
) {

    init {
        val authors = authorRepository.saveAll(
            (1..2).map { Author("AuthorFirstName$it", "AuthorLastName$it") }
        )
        bookRepository.saveAll(
            (1..4).map {
                Book("Book $it", authors[if (it <= 2) 0 else 1])
            }
        )
    }
}
