package com.lv_spring.data.rest.jpa.demo.repositories;

import com.lv_spring.data.rest.jpa.JpaRepositoryAndSpecificationExecutor
import com.lv_spring.data.rest.jpa.demo.models.Author

interface AuthorRepository : JpaRepositoryAndSpecificationExecutor<Author, Long>
